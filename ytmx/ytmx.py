#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2022 Felix Delattre <felix@delattre.de>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

import sys
import os
import subprocess
from tempfile import TemporaryDirectory
import yt_dlp
from yt_dlp.utils import sanitize_filename
from PIL import Image
import urllib
from shutil import which

COVER_IMG = "cover.jpg"


class MissingDependencyWarning(Exception):
    def __init__(self, dependency, description=""):
        self.dependency = dependency
        self.description = description

    def __str__(self):
        return f"Warning: {self.dependency} is missing! {self.description}"


class CommandFailed(Exception):
    def __init__(self, command, output):
        self.command = " ".join(command)
        self.output = output

    def __str__(self):
        return f"Couldn't extract cover: command: {self.command}\n{self.output}"


class InvalidRatio(Exception):
    def __init__(self, ratio):
        self.ratio = ratio

    def __str__(self):
        return f"Video has wrong aspect ratio to be a thumbnail (ratio={self.ratio})."


def unpack(d, keys):
    return tuple((d[k] if k in d else str()) for k in keys)


def reprint(arg, **kwargs):
    # move up one line, move cursor to start, erase line
    print("\r\033[A\33[2K" + arg, **kwargs)


def make_new_folder_name(base):
    count = 1
    folder = base
    while os.path.exists(folder):
        folder = f"{base}_{count}"
        count += 1
    return folder


def extract_thumbnail_from_video(link: str):
    if not which("mpv"):
        raise MissingDependencyWarning(
            "mpv",
            description="You will get better thumbnails (up to 1080x1080) with mpv.",
        )

    mpv_link = link if link.startswith("https://") else f"ytdl://{link}"
    cmd = [
        "mpv",
        mpv_link,
        "--ytdl-raw-options=playlist-end=1",
        "--quiet",
        "--start=0",
        "--aid=no",
        "--vo=image",
        "--vf=scale=out_range=pc",
        "--ovcopts=qmin=2",
        "--ovcopts=qmax=3",
        "--frames=1",
        f"-o={COVER_IMG}",
    ]
    output = subprocess.run(cmd, capture_output=True, encoding="UTF-8")

    if output.returncode != 0 or not os.path.exists(COVER_IMG):
        raise CommandFailed(cmd, output.stdout)

    # check whether image has the correct format (some older youtube music
    # videos show the cover in small + title/album name)
    image = Image.open(COVER_IMG)
    width, height = image.size
    ratio = width / height
    image.close()
    if ratio < 0.9 or ratio > 1.1:
        raise InvalidRatio(ratio=ratio)


def download_yt_thumbnail(playlist_entries):
    # remove if some cover was downloaded already
    if os.path.exists(COVER_IMG):
        os.remove(COVER_IMG)

    # last thumbnail is the largest
    thumbnail = playlist_entries[0]["thumbnails"][-1]
    urllib.request.urlretrieve(thumbnail["url"], COVER_IMG)

    # crop to 1:1
    cover = Image.open(COVER_IMG)
    width, height = cover.size
    crop_pixels = int((width - height) * 0.5)
    cover.crop((crop_pixels, 0, crop_pixels + height, height)).save(
        COVER_IMG, quality=96
    )


class YTMX:
    downloading = False

    def __init__(self, links):
        for link in links:
            self.download_album(link)

    def download_album(self, link):
        print("Downloading tracks...")

        # get temp dir name
        tempDir = ""
        with TemporaryDirectory(dir="./", prefix="ytmx-") as tmpDir:
            tempDir = tmpDir

        os.mkdir(tempDir)
        os.chdir(tempDir)

        ytdl = yt_dlp.YoutubeDL(
            params={
                "quiet": True,
                "format": "bestaudio",
                "outtmpl": "%(playlist_index)02d.%(ext)s",
                "progress_hooks": [lambda values: self.progress_hook(values)],
                "postprocessors": [{"key": "FFmpegMetadata"}],
            }
        )
        playlistInfo = ytdl.extract_info(link)

        entries = playlistInfo["entries"]

        # determine correct release year (tracks might have different years)
        try:
            album_year = max(entry["release_year"] for entry in entries)
        except TypeError:
            album_year = False
        album_title = entries[0]["album"]
        album_artist = entries[0]["artist"].split(", ")[0].split(" feat. ")[0]

        # create new album folder
        os.chdir("..")
        album_filename = sanitize_filename(f"{album_title}", restricted=True)
        if album_year:
            album_filename += f"_({album_year})"
        album_folder = make_new_folder_name(album_filename)
        os.rename(tempDir, album_folder)
        os.chdir(album_folder)

        # Extract cover picture from video
        # (the video often has a better res than the yt thumbnail)
        reprint("Extracting cover picture...")
        try:
            extract_thumbnail_from_video(link)
        except Exception as e:
            # use youtube thumbnail instead
            print(str(e))
            print("Falling back to YouTube thumbnail (720x720).")
            download_yt_thumbnail(entries)

        # Tagging files
        reprint("Tagging files...")

        # Put opus files into ogg for android mediaserver (doesn't support .opus)
        for i in range(len(entries)):
            if entries[i]["ext"] == "webm":
                os.system(
                    f"ffmpeg -loglevel quiet -i {(i+1):02}.webm -c copy {(i+1):02}.ogg"
                )
                os.remove(f"{(i+1):02}.webm")
                entries[i]["ext"] = "ogg"

        # Tag using kid3-cli
        tasks = [
            "numbertracks",
            f'set "Album Artist" "{album_artist}"',
            f'set picture:"./cover.jpg" "Cover"',
        ]
        if album_year:
            tasks.append(f'set "Date" "{album_year}"')
        command = f"kid3-cli -c select"
        # add all files to selection
        for i in range(len(entries)):
            ext = entries[i]["ext"]
            command += f" {(i+1):02}.{ext}"
        for task in tasks:
            command += f" -c '{task}'"

        os.system(command)

        # Rename files
        for i in range(len(entries)):
            ext = entries[i]["ext"]
            title = entries[i]["title"]
            base_name = sanitize_filename(f"{(i+1):02}_-_{title}", restricted=True)
            # remove "_" from the end
            if base_name[-1] == "_":
                base_name = base_name[:-1]

            os.rename(f"{(i+1):02}.{ext}", f"{base_name}.{ext}")

        os.chdir("..")
        # clear command line
        reprint("", end="", flush=True)

    def progress_hook(self, values):
        if values["status"] == "finished":
            self.downloading = False
        elif values["status"] == "downloading":
            if not self.downloading:
                self.downloading = True
                title, album, artist, track_index, track_count = unpack(
                    values["info_dict"],
                    ("title", "album", "artist", "playlist_index", "playlist_count"),
                )
                reprint(
                    f"Downloading {track_index}/{track_count} '{title}' from '{album}' by '{artist}'"
                )


def main():
    if len(sys.argv) <= 1:
        print("Usage: " + sys.argv[0] + " URL [URL...]")
        exit(1)

    YTMX([sys.argv[i] for i in range(1, len(sys.argv))])


if __name__ == "__main__":
    main()
