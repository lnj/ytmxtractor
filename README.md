<!--
SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>

SPDX-License-Identifier: CC0-1.0
-->

# YTMX

Usage:
```bash
ytmx https://music.youtube.com/playlist?list=OLAK5uy_mdFMT_6jHeXEFAzkrbGszwqbrJQzYs2B4
```
This will download the album into a new folder named "&lt;title&gt;_(&lt;year&gt;)" with all tracks
correctly numbered and with the correct tagging including the cover.

### Installation

 * `pip install .`

Make sure you have the following non-python dependencies on your system:
 * yt-dlp
 * mpv (for extracting the screenshot from the video (the video has a higher resolution than the thumbnail image))
 * kid3-cli (for tagging)

## FAQ

### Why does this output .ogg files? YouTube doesn't offer the Ogg Vorbis codec.

YouTube mainly offers Opus and AAC audio formats.  YTMX does not reencode the audio.  AAC ends up
in .m4a and Opus in .ogg containers.

The Android media server doesn't recognize .opus files as audio files (although Opus is generally
supported).  The trick to fix this was to put the Opus audio into an .ogg container or what also
worked was just renaming the file to .ogg.

This might not be needed with newer Android versions anymore. I only tested it with Android Oreo
(8.0).

### Why are some downloads in Opus and some in AAC?

YTMX uses yt-dlp to download the actual audio.  yt-dlp picks the (quality-wise) best version
offered by YouTube.  The picked format may vary.

