#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2022 Felix Delattre <felix@delattre.de>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

from setuptools import setup, find_packages

setup(
    name="ytmx",
    version="0.1",
    description="YouTube Music Extractor for downloading Albums and tagging them nicely",
    license="GPL-3.0-only",
    install_requires=[
        "Pillow",
    ],
    packages=find_packages(),
    entry_points={"console_scripts": ["ytmx = ytmx.ytmx:main"]},
    python_requires=">=3.8",
)
